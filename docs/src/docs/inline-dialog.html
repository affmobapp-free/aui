---
component: Inline dialog
layout: main-layout.html
---

<a href="https://design.atlassian.com/latest/product/components/inline-dialog/" class="aui-button aui-button-link" id="adg-button">Design guidelines</a>
<div class="aui-message aui-message-warning">
    <strong>Inline dialog has been deprecated</strong> and will be removed in a future version.
    <ul class="aui-nav-actions-list"><li><a href="inline-dialog2.html">View Inline dialog 2 documentation</a></li></ul>
</div>
<h3>Summary</h3>
<p>The inline dialog is a wrapper for secondary content/controls to be displayed on user request. Consider this component as displayed in context to the triggering control with the dialog overlaying the page content.</p>
<p>A inline dialog should be preferred over a modal dialog when a connection between the action has a clear benefit versus having a lower user focus.</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
        <tr>
            <th>API status:</th>
            <td><span class="aui-lozenge aui-lozenge-success">general</span>
            </td>
        </tr>
        <tr>
            <th>Included in AUI core?</th>
            <td>Yes. You do not need to explicitly require the web resource key.</td>
        </tr>
        <tr>
            <th>Web resource key:</th>
            <td class="resource-key" data-resource-key="com.atlassian.auiplugin:ajs"><code>com.atlassian.auiplugin:ajs</code>
            </td>
        </tr>
        <tr>
            <th>AMD Module key:</th>
            <td class="resource-key">N/A</td>
        </tr>
        <tr>
            <th>Experimental since:</th>
            <td>1.0</td>
        </tr>
    </tbody>
</table>

<h3>Examples</h3>
<div class="aui-flatpack-example inlinedialog-example">
    <p><a href="http://example.com/" id="popupLink">Show inline dialog</a>
    </p>
    <script>
        AJS.InlineDialog(AJS.$("#popupLink"), "myDialog",
            function(content, trigger, showPopup) {
                content.css({
                    "padding": "20px"
                }).html(
                    '<h2>Inline dialog</h2><p>The inline dialog is a wrapper for secondary content/controls to be displayed on user request. Consider this component as displayed in context to the triggering control with the dialog overlaying the page content.</p>'
                );
                showPopup();
                return false;
            }
        );
    </script>
</div>
<h3>Code</h3>
<h4>HTML and JavaScript</h4>
<p>The HTML component is simply a trigger with an ID, which can be used by Inline Dialog's JavaScript.</p>

<aui-docs-code>
    <a href="#" id="inlineDialog">Inline dialog</a>
</aui-docs-code>

<p>To inject content from a URL:</p>

<aui-docs-code lang="js">
    AJS.InlineDialog(AJS.$("#inlineDialog"), "myDialog", "path/to/your/content.html");
</aui-docs-code>

<p>This binds an InlineDialog to the trigger element with id="inlineDialog". Clicking the trigger element displays a container element with <code>id="inline-dialog-myDialog"</code> and sets its innerHTML value to the content fetched from "dialog-content.html".
    NOTE: "content.html" could have something like the following HTML: <code>&lt;div id="dialog-content"&gt;&lt;h2&gt;AUI Inline Dialog&lt;/h2&gt;&lt;/div&gt;</code>
</p>
<p>To inject content directly with JavaScript:</p>

<aui-docs-example>
    <aui-docs-example-html>
        <a href="#" id="inlineDialog">Inline dialog</a>
    </aui-docs-example-html>
    <aui-docs-example-js>
        AJS.InlineDialog(jQuery('#inlineDialog'), 'myDialog', function(content, trigger, showPopup) { content.css({ padding: '20px' }).html(
            '<h2>Inline dialog</h2>' +
            '<p>Content.</p>');
            showPopup();
            return false;
        });
    </aui-docs-example-js>
</aui-docs-example>

<h4>Primary instantiation function</h4>
<p>AUI Inline Dialog uses a function to attach and inline-dialog to a DOM element.</p>

<aui-docs-code lang="js">
    AJS.InlineDialog(items, identifier, url, options);
</aui-docs-code>

<h4>Parameters</h4>
<ul>
    <li><strong>items</strong>&nbsp;- The elements that will trigger the inline-dialog, use a jQuery Selector to select items.</li>
    <li><strong>identifier</strong>&nbsp;- A unique identifier for the inline-dialog.</li>
    <li><strong>url</strong>&nbsp;- The URL of the dialog contents</li>
    <li><strong>options</strong>&nbsp;- A number of different options may be passed in as an associative array, options explained below.</li>
</ul>
<h4>Default behavior</h4>
<ul>
    <li>If no options are changed the Inline Dialog will be default display below the trigger aligned to the left of the trigger.</li>
    <li>If the dialog is drawn offscreen to the right it will be re-positioned such that it is on-screen.</li>
    <li>If the dialog is drawn off-screen to the bottom it will be displayed above the trigger with the arrow flipped.</li>
    <li>In all cases the arrow will be drawn in the middle of the trigger.</li>
</ul>
<h4>Advanced usage</h4>
<h4>Passing a function instead of a URL String</h4>
<p>You can pass a function into the url parameter of inlineDialog instead of a url string. Your function must be:</p>

<aui-docs-code lang="js">
    function(contents, trigger, showPopup);
</aui-docs-code>

<p>Where:</p>
<ul>
    <li><strong>Contents</strong>&nbsp;is the div element that will contain your custom content</li>
    <li><strong>trigger</strong>&nbsp;is the element of your dialog trigger</li>
    <li><strong>showPopup</strong>&nbsp;is the function from within InlineDialog that shows the popup (your function should call this at the end)</li>
</ul>
<p>This function will override the contents loading section in Inline-Dialog.</p>
<p>Quick example:</p>

<aui-docs-code lang="js">
    AJS.InlineDialog(jQuery('#inlineDialog'), 'myDialog', function(content, trigger, showPopup) { // ... });
</aui-docs-code>

<h4 id="InlineDialog-Options">Options</h4>
<p>To pass options into the InlineDialog function you must use an associative array:</p>

<aui-docs-code lang="js">
    AJS.InlineDialog(jQuery('#inlineDialog'), 'myDialog', 'dialog-content.html', { optionName: optionValue });
</aui-docs-code>

<p>The following options can be passed in as an object in the fourth parameter of an Inline Dialog instantiation.</p>

<aui-docs-code lang="js">
    AJS.InlineDialog( jQuery('#inlineDialog'), 'myDialog', 'dialog-content.html', { onHover: true, noBind true, fadeTime: 500, initCallback: function() { alert('Hello World!'); } } );
</aui-docs-code>

<table class="aui">
    <thead>
        <tr>
            <th>Option</th>
            <th>Details</th>
            <th>Examples</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>onHover</code>
            </td>
            <td>
                <p>determines whether the inline-Dialog will show on a mouseOver or mouseClick of the trigger.
                    <br>Options: true / false
                    <br>Default: false</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    onHover: true
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>closeOnTriggerClick</code>
            </td>
            <td>
                <p>determines if the the inline dialog is closed when its trigger is clicked
                    <br>Options: true / false
                    <br>Default: false</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    closeOnTriggerClick: true
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>noBind</code>
            </td>
            <td>
                <p>determines if the inline-Dialog will bind the event-handler to the trigger event, use this option if you wish to bind the event handler programatically at a different point. If <code>true</code> then <code>useLiveEvents</code> is disabled.
                    If <code>true</code> then the <code>items</code> argument to the inline dialog constructor can be the empty jQuery collection, i.e., <code>AJS.$()</code>.
                    <br>Options: true / false
                    <br>Default: false</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    noBind: true
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>fadeTime</code>
            </td>
            <td>
                <p>determines the fade in and fade out duration of the dialog in milliseconds.
                    <br>Accepts a numerical value.
                    <br>Default: 100</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    fadeTime: 200
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>hideDelay</code>
            </td>
            <td>
                <p>determines how long (in milliseconds) the inline-dialog will stay visible for until it is hidden (if no other trigger for hiding the dialog is fired) if null is passed auto-hide is disabled for this inline-Dialog
                    <br>Accepts a Numerical value.
                    <br>Default: 10000 (or null)</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    hideDelay: 3000
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>showDelay</code>
            </td>
            <td>
                <p>determines how long in milliseconds after a show trigger is fired (such as a trigger click) until the dialog is shown.
                    <br>Accepts a Numerical Value
                    <br>Default: 0</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    showDelay: 200
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>width</code>
            </td>
            <td>
                <p>Sets how wide the inline-dialog is in pixels.
                    <br>Accepts a Numerical value
                    <br>Default: 300</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    width: 400
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>offsetX</code>
            </td>
            <td>
                <p>Sets an offset distance of the inline-dialog from the trigger element along the x-axis in pixels.</p>
                <p>Accepts a Numerical Value or a function that takes the same arguments as <code>calculatePositions</code> and returns a numeric value.</p>
                <p>Default: <code>0</code> for <code>'n'</code> and <code>'s'</code> gravity; <code>10</code> for <code>'w'</code> gravity</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    offsetX: 30
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>offsetY</code>
            </td>
            <td>
                <p>Sets an offset distance of the inline-dialog from the trigger element along the y-axis in pixels.</p>
                <p>Accepts a Numerical Value or a function that takes the same arguments as <code>calculatePositions</code> and returns a numeric value.</p>
                <p>Default: <code>10</code> for <code>'n'</code> and <code>'s'</code> gravity; <code>0</code> for <code>'w'</code> gravity</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    offsetY: 30
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>container</code>
            </td>
            <td>
                <p>The element in which the dialog itself will be appended.</p>
                <p>Accepts a String or an element.</p>
                <p>Default: "body"</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    container: 'head'
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>cacheContent</code>
            </td>
            <td>
                <p>determines if the contents of the dialog are cached. If set to false the contents will be reloaded everytime the dialog is shown.</p>
                <p>Options: true / false</o>
                    <p>Default: true</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    cacheContent: false
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>hideCallback</code>
            </td>
            <td>
                <p>a function that will be called after the popup has faded out.</p>
                <p>Accepts a javascript function.</p>
                <p>Default: function () {}</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    hideCallback: function() { alert('Hello World!'); }
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>initCallback</code>
            </td>
            <td>
                <p>a function that will be called after the popup contents have been loaded.</p>
                <p>Accepts a javascript function.</p>
                <p>Default: function(){}</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    initCallback: function() { alert('Hello World!'); }
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>isRelativeToMouse</code>
            </td>
            <td>
                <p>determines if the dialog should be shown relative to where the mouse is at the time of the event trigger (normally a click) if set to false the dialog will show aligned to the left of the trigger with the arrow showing at the center.</p>
                <p>Options: true / false</p>
                <p>Default: false</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    isRelativeToMouse: true
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>closeOthers</code>
            </td>
            <td>
                <p>determines if all other dialogs on the screen are closed when this one is opened.</p>
                <p>Options: true / false</p>
                <p>Default: true</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    closeOthers: true
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>responseHandler</code>
            </td>
            <td>
                <p>A function that determines how the content retrieval response is handled, the default assumes that the data returned is html. The implemented function must handle the three variables: data, status, xhr and at the end return html.</p>
                <p>Accepts a javascript Function
                    <p>Default: function(data, status, xhr) { return data; }</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    responseHandler: function(data, status, xhr) { return doSomethingWithTheData(data); }
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>onTop</code> <span class="aui-lozenge aui-lozenge-error">Deprecated as of 5.5</span>
            </td>
            <td>
                <p>determines if the dialog should be shown above the trigger or not. If this option is true but there is insufficient room above the trigger the inline-dialog will be flipped to display below it.</p>
                <p>Deprecated in favour of <code>gravity</code>.</p>
                <p>Options: true / false</p>
                <p>Default: false</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    onTop: true
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>gravity</code>
            </td>
            <td>
                <p>As of 5.5: The preferred direction in which the inline dialog's arrow points, e.g., <code>'n'</code> makes the inline dialog's arrow point up (with the inline dialog below the trigger). When there isn't enough room to display the inline
                    dialog with its preferred gravity it automatically flips orientation.</p>Options: <code>'n'</code>, <code>'s'</code>, or <code>'w'</code>.</p>
                <p>Default: <code>'n'</code>
                </p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    gravity: 'n'
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>useLiveEvents</code>
            </td>
            <td>
                <p>AUI supports jQuery <a href="http://api.jquery.com/live/" class="external-link" rel="nofollow" target="_blank">live events</a>. If you choose this option, AUI will bind all events on the page to the HTML body element instead of to each
                    individual element. This means that your events can be bound to all current elements and to elements that do not yet exist. You no longer need to rebind everything on an Ajax load. This is essential on a page which has, for example,
                    a number of user avatars that react on hover. Binding can cause a performance problem when there is a large number of such elements. Disabled if <code>noBind</code> is <code>true</code>.</p>
                <p>Options: true / false</p>
                <p>Default: false</p>
            </td>
            <td>
                <p>See <a href="/display/AUI/AUI+3.1+Release+Notes">AUI 3.1 Release Notes</a>.</p>
            </td>
        </tr>
        <tr>
            <td><code>displayShadow</code>
            </td>
            <td>
                <p>Instructs the InlineDialog on rendering the shadow.</p>
                <p>Options: true / false</p>
                <p>Default: true</p>
            </td>
            <td>
                <p>See&nbsp;<a href="/display/AUI/AUI+3.5+Release+Notes">AUI 3.5 Release Notes</a>
                </p>
            </td>
        </tr>
        <tr>
            <td><code>getArrowPath</code> <span class="aui-lozenge aui-lozenge-current">Deprecated as of 5.1</span>
            </td>
            <td>
                <p>A function that returns a string representation of an SVG path that will represent the arrow being drawn. This function takes one argument, positions which is the return value of the calculatePositions method.</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    getArrowPath: function (positions) { return positions.displayAbove ? 'M 0 0 L 0 0 0 8 8 8 8 0 0 0' : 'M 0 0 L 0 0 0 8 8 8 8 0 0 0'; }
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>getArrowAttributes</code> <span class="aui-lozenge aui-lozenge-error">Deprecated as of 5.1</span>
            </td>
            <td>
                <p>Returns an object which has attributes to be applied to the arrow svg element.</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    getArrowAttributes: function() { return { fill: '#ff0000', stroke: '#000000' }; }
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>addActiveClass</code>
            </td>
            <td>
                <p>Instructs the InlineDialog to add the 'active' class.</p>
                <p>Options: true / false</p>
                <p>Default: true</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    addActiveClass: false
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>calculatePositions</code>
            </td>
            <td>
                <p>Allows the consumer of InlineDialog to manually determine or calculate the position that the InlineDialog should be drawn at.</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    calculatePositions: function getPosition(popup, targetPosition, mousePosition, opts) { return { displayAbove: true, popupCss: { left: mousePosition.x, top: mousePosition.y + 20, right: mousePosition.y + 100 }, arrowCss: { left: 20, top: 0, right: 20 }
                    }; }
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>arrowOffsetX</code>
            </td>
            <td>
                <p>As of 5.0; <code>arrowOffsetX</code> defines an X axis offset in pixels for placement of the arrow (default is zero).</p>
                <p>Accepts a Numerical Value or a function that takes the same arguments as <code>calculatePositions</code> and returns a numeric value.</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    arrowOffsetX: 50
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>arrowOffsetY</code>
            </td>
            <td>
                <p>As of 5.5; <code>arrowOffsetY</code> defines a Y axis offset in pixels for placement of the arrow (default is zero).</p>
                <p>Accepts a Numerical Value or a function that takes the same arguments as <code>calculatePositions</code> and returns a numeric value.</p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    arrowOffsetY: 50
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td><code>persistent</code>
            </td>
            <td>
                <p>As of 5.1: if persistent: true the inline dialog can only be dismissed programatically by calling .hide(). This option, ignores the 'closeOthers' option. (inline-dialogs with closeOthers set to true will not close this one) and the hideDelay
                    option.
                </p>
            </td>
            <td>
                <aui-docs-code lang="js">
                    persistent: true
                </aui-docs-code>
            </td>
        </tr>
    </tbody>
</table>
<h4>JavaScript functions</h4>
<p>The following functions have been available since AUI 3.0. They can be called on the DOM object returned by the Inline Dialog constructer. You can call these by assigning the constructor to a DOM object such as:</p>

<aui-docs-code lang="js">
    var inlineDialog1 = AJS.InlineDialog(jQuery('#inlineDialog'), 'myDialog', 'dialog-content.html');
</aui-docs-code>

<p>...after that you can call them directly from the object (as per the table below).</p>
<table class="aui">
    <thead>
        <tr>
            <th>Function</th>
            <th>Details</th>
            <th>Example</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>show(e, trigger)</td>
            <td>Shows the Inline Dialog. As of 5.5: Accepts two optional parameters: an event, <code>e</code>, and an element, <code>trigger</code>. If the <code>noBind</code> option is <code>true</code> (when binding your own event handlers) <em>and</em>                the <code>items</code> collection passed to the inline dialog constructor is empty, pass in the event object from your event handler as <code>e</code> and, optionally, pass your desired trigger element as <code>trigger</code>. If <code>e</code>                is provided but <code>trigger</code> is not, <code>e.target</code> is used as the trigger element.</td>
            <td>
                <aui-docs-code lang="js">
                    inlineDialog1.show();
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td>hide()</td>
            <td>Hides the Inline Dialog</td>
            <td>
                <aui-docs-code lang="js">
                    inlineDialog1.hide();
                </aui-docs-code>
            </td>
        </tr>
        <tr>
            <td>refresh()</td>
            <td>Redraws the inline-dialog. Use this function when you need to add contents to the inline dialog and you need it to be redrawn after your contents are inserted.</td>
            <td>
                <aui-docs-code lang="js">
                    inlineDialog1.refresh();
                </aui-docs-code>
            </td>
        </tr>
    </tbody>
</table>
