//This module needs an explicit name so that in dev mode we can include this
// file with a script tag and then require the module as per production.

define('scripts/index', [
    'jquery',
    'components/code',
    'components/example-view'
], function (
    $
    ) {
    'use strict';

    function makeBitbucketChangelogUrl (version) {
        var baseUrl = 'https://bitbucket.org/atlassian/aui/src/master/changelog.md#markdown-header-';
        var strippedVersionNumber = version.replace(/\./g, '');
        return baseUrl + strippedVersionNumber;
    }

    $(function() {
        // Highlight the selected nav item.
        var url = window.location.pathname;
        var currentPage = url.substring(url.lastIndexOf('/') + 1);
        var navItem = document.querySelector('a[href="' + currentPage + '"]');

        if (navItem) {
            $(navItem.parentNode).addClass('aui-nav-selected');
        }

        AJS.log('Like great design and digging into the code? We\'re hiring! http://bit.ly/Y9xoQu');

        // Dialog handlers.
        AJS.$('#cdn-button').click(function() {
            AJS.dialog2('#cdn-dialog').show();
        });
        AJS.$('#cdn-dialog-close').click(function() {
            AJS.dialog2('#cdn-dialog').hide();
        });

        AJS.$('#download-flatpack-link').click(function() {
            AJS.dialog2('#download-dialog').show();
        });
        AJS.$('#download-dialog-close').click(function() {
            AJS.dialog2('#download-dialog').hide();
        });

        if(window.location.hash === '#sandboxRedirect') {
            require(['aui/flag'], function(flag) {
                var myFlag = flag({
                    type: 'info',
                    title: 'We\'re now using JS Bin',
                    close: 'manual',
                    body: 'Find code samples for each component in the documentation. If you want to hack on a component, click the "Edit in JS Bin" link next to the code sample.'
                });
            });
        }

        // Changelog links.
        AJS.$('.aui-docs-changelog-link').attr('href', makeBitbucketChangelogUrl(AJS.version));
    });
});

require(['scripts/index']);