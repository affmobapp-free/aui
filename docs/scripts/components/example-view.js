define([
    '../../bower_components/skatejs/dist/skate',
    'sandy',
    'components/code'
], function (skate, Sandy, code) {
    'use strict'

    skate('aui-docs-example', {
        type: skate.types.NOCLASS,
        template: function(el) {
            var oldElement;
            if (el.tagName === 'SCRIPT') {
                oldElement = el;
                el = document.createElement('div');
                el.innerHTML = oldElement.innerHTML;
                el.className = 'aui-docs-example';
                oldElement.parentNode.replaceChild(el, oldElement);
            }

            var singleLanguage = el.hasAttribute('lang');
            var languages = {};

            if (singleLanguage) {
                var language = el.getAttribute('lang');
                languages[language] = el.innerHTML;
            } else {
                languages.html = getExampleHtml(el);
                languages.js = el.querySelector('aui-docs-example-js') && el.querySelector('aui-docs-example-js').innerHTML;
                languages.css = el.querySelector('aui-docs-example-css') && el.querySelector('aui-docs-example-css').innerHTML;
            }

            clearCodeContents(el);

            var sandyExample = createSandyExample(languages.html, languages.js, languages.css);

            var exampleView = createExampleView(languages.html, languages.js, languages.css);
            el.appendChild(exampleView);

            var exampleActions = createExampleActions(sandyExample);
            el.appendChild(exampleActions);
        }
    });

    function getExampleHtml(el) {
        var exampleHtmlElement = el.querySelector('aui-docs-example-html');
        var exampleHtmlElementInTextarea = el.querySelector('textarea[aui-docs-example-html]');
        if (exampleHtmlElement) {
            return exampleHtmlElement.innerHTML;
        } else if (exampleHtmlElementInTextarea) {
            return exampleHtmlElementInTextarea.value;
        }
    }

    function clearCodeContents(el) {
        el.innerHTML = '';
    }

    function createExampleActions(sandyExample) {
        var id = AJS.id('aui-docs-example-actions-dropdown');
        var exampleActionsContent =
        '<div class="aui-buttons">' +
        '    <button class="aui-button aui-button-split-main aui-button-light" data-docs-example-destination="jsbin"><span class="aui-icon aui-icon-small aui-iconfont-edit">Edit icon</span> Edit in jsbin</button>' +
        '    <button class="aui-button aui-dropdown2-trigger aui-button-split-more aui-button-light" aria-haspopup="true" aria-controls="' + id + '" aria-expanded="false">Split more</button>' +
        '</div>' +
        '<div id="' + id + '" class="aui-dropdown2 aui-style-default aui-layer" aria-hidden="true">' +
        '    <ul class="aui-list-truncate">' +
        '        <li><a data-docs-example-destination="codepen" href="#">Edit in codepen</a></li>' +
        '        <li><a data-docs-example-destination="jsfiddle" href="#">Edit in jsfiddle</a></li>' +
        '    </ul>' +
        '</div>';

        var exampleActions = document.createElement('div');
        exampleActions.className = 'aui-docs-example-actions';
        exampleActions.innerHTML = exampleActionsContent;

        var buttons = exampleActions.querySelectorAll('[data-docs-example-destination]');

        function exampleButtonClickHandler(e){
            sandyExample.pushTo(e.target.getAttribute('data-docs-example-destination'));
        }
        for(var i = 0; i < buttons.length; i++){
            buttons[i].addEventListener('click', exampleButtonClickHandler);
        }

        return exampleActions;
    }

    function createExampleView(html, js, css) {
        var exampleView = document.createElement('div');

        if (html) {
            var htmlCode = createCodeBlock('html', html);
            exampleView.appendChild(htmlCode);
        }

        if (css) {
            var cssCode = createCodeBlock('css', css);
            exampleView.appendChild(cssCode);
        }

        if (js) {
            var jsCode = createCodeBlock('javascript', js);
            exampleView.appendChild(jsCode);
        }

        return exampleView;
    }

    function createCodeBlock(language, content) {
        var newCodeBlock = document.createElement('aui-docs-code');
        newCodeBlock.setAttribute('lang', language);
        newCodeBlock.innerHTML = content;
        return newCodeBlock;
    }

    function createSandyExample(html, js, css) {
        var config = {
            dependencies: getDependencies()
        };

        if (html) {
            config.html = {content: html};
        }

        if (css) {
            config.css = {content: css};
        }

        if (js) {
            config.js = {content: js};
        }

        return new Sandy(config);


    }

    function getDependencies() {
        var distLocation = getDistLocation();

        var dependencies = [
            'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js',
            'http://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.15/require.min.js',
            distLocation + 'js/aui.js',
            distLocation + 'js/aui-experimental.js',
            distLocation + 'js/aui-datepicker.js',
            distLocation + 'css/aui.css',
            distLocation + 'css/aui-experimental.css'
        ];

        return dependencies;
    }

    function getDistLocation() {
        //Inserted by the template at run time based on the presence / absence of the --debug flag
        return document.getElementById('dist-location').innerHTML;
    }
});