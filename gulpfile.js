var del = require('del'),
    gulp = require('gulp');

var args = require('./build/gulp/args'),
    paths = require('./build/gulp/paths');

require('./build/gulp/css');
require('./build/gulp/docs');
require('./build/gulp/flatapp');
require('./build/gulp/js');
require('./build/gulp/lint');
require('./build/gulp/test');

gulp.task('build', ['js', 'css']);

gulp.task('clean', function(cb) {
    del([paths.tmp, paths.dist, paths.docsDist, paths.flatappBuild], cb);
});

gulp.task('watch', function() {
    gulp.watch(['src/js/**', 'src/js-vendor/**', 'src/soy/**', 'src/i18n/**'], ['js']);
    gulp.watch(['src/less/**', 'src/css-vendor/**'], ['css']);
});
