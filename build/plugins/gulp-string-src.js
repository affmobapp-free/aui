var gutil = require('gulp-util'),
    stream = require('stream');

/**
 * Returns a stream with the given filename and file contents
 */
module.exports = function stringSrc(filename, contents) {
    var src = require('stream').Readable({ objectMode: true });
    src._read = function () {
        this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(contents) }));
        this.push(null)
    };
    return src;
};