var gutil       = require('gulp-util'),
    requirejs   = require('requirejs'),
    es          = require('event-stream'),
    PluginError = gutil.PluginError;

const PLUGIN_NAME = 'gulp-aui-requirejs-2';

module.exports = function(opts) {

    'use strict';

    if (!opts) {
        throw new PluginError(PLUGIN_NAME, 'Missing options array!');
    }

    if (!opts.out && typeof opts.out !== 'string') {
        throw new PluginError(PLUGIN_NAME, 'Only single file outputs are supported right now, please pass a valid output file name!');
    }

    if (!opts.baseUrl) {
        throw new PluginError(PLUGIN_NAME, 'Pipeing dirs/files is not supported right now, please specify the base path for your script.');
    }

    var fileContents;
    var filename = opts.out;
    opts.optimize = 'none';
    opts.out = function(text, sourceMapText) {
        fileContents = text;
    };
    requirejs.optimize(opts);

    return es.readable(function(count, callback) {
        if (fileContents) {
            this.emit('data', new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(fileContents) }));
            this.emit('end');
        }
        callback();
    });
};
