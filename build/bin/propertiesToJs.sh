#!/bin/bash

#process properties file
awk -F= '
BEGIN {
    print "AJS.I18n.keys = {};"
}
{
    gsub(/ /,"",$1);
    gsub(/^[ ]+/,"",$2);
    gsub(/\"/, "\\\"", $2);
    print "AJS.I18n.keys[\""$1"\"] = \"" $2 "\";";
}
END {

}
'