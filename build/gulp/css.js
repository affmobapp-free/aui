var gulp = require('gulp'),
    less = require('gulp-less'),
    merge = require('merge-stream'),
    minifyCss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    paths = require('./paths');

gulp.task('css:dist', function () {
    return merge(
        // less + minify
        gulp.src('src/less/batch/*.less')
            .pipe(less({
                ieCompat: true,
                relativeUrls: true
            }))
            .pipe(gulp.dest('dist/aui/css'))
            .pipe(rename({suffix: '.min'}))
            .pipe(minifyCss())
            .pipe(gulp.dest('dist/aui/css')),

        // copy less images
        gulp.src('src/less/images/**')
            .pipe(gulp.dest('dist/aui/css')),

        // select2 images
        gulp.src(['src/css-vendor/jquery/plugins/*.png', 'src/css-vendor/jquery/plugins/*.gif'])
            .pipe(gulp.dest('dist/aui/css'))
    );
});

gulp.task('css', ['css:dist'], function() {
    return gulp.src('dist/aui/css/**')
        // docs
        .pipe(gulp.dest(paths.docsDist + '/aui/css'))
        // flatapp
        .pipe(gulp.dest(paths.flatappBuild + '/static/common/css'));
});