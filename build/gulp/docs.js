var gulp = require('gulp'),
    fs = require('fs-extra'),
    pkg = require('../../package.json'),
    webserver = require('gulp-webserver');

var args = require('./args'),
    createDocsTask = require('../tasks/docs');

gulp.task('docs:copy', ['js', 'css'], function() {
    var requireJsConfig = require('../configs/requirejs').docs;

    return createDocsTask({
        requireJsConfig: requireJsConfig,
        templateSrc: ['docs/src/**/*.html'],
        assetSrc: ['docs/src/**', '!docs/src/**/*.html'],
        out: 'dist-docs',
        pkgVersion: pkg.version,
        distLocation: (args.debug ? '//localhost:8000/aui/' : '//aui-cdn.atlassian.com/aui/' + pkg.version + '/')
    });
});

gulp.task('docs:watch', function() {
    gulp.watch(['docs/scripts/**', 'docs/src/**', 'docs/templates/**'], ['docs:copy']);
});

gulp.task('docs', ['watch', 'docs:watch', 'docs:copy'], function() {
    return gulp.src('dist-docs')
        .pipe(webserver({
            livereload: true,
            open: true,
            port: 8000
        }));
});