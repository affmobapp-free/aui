var extend = require('node.extend'),
    gulp = require('gulp'),
    fs = require('fs-extra'),
    merge = require('merge-stream'),
    pkg = require('../../package.json'),
    soyRender = require('../tasks/soy-render'),
    webserver = require('gulp-webserver');

var paths = require('./paths');

gulp.task('flatapp:copy', ['js', 'css'], function() {
    return merge(
        gulp.src('tests/flatapp/src/soy/**/*.soy')
            .pipe(gulp.dest(paths.flatappBuild + '/soy/')),

        // Other static source files
        gulp.src('tests/flatapp/src/static/**')
            .pipe(gulp.dest(paths.flatappBuild + '/static')),

        // Soy test pages
        gulp.src('tests/test-pages/pages/**/*.soy')
            .pipe(gulp.dest(paths.flatappBuild + '/soy/pages')),

        // Static test pages
        gulp.src(['tests/test-pages/pages/**', '!**/*.soy'])
            .pipe(gulp.dest(paths.flatappBuild + '/static/pages')),

        // The common soy files for the test pages
        gulp.src('tests/test-pages/common/**/*.soy')
            .pipe(gulp.dest(paths.flatappBuild + '/soy/dependencies')),

        // The common html and js files go straight to the target directory
        gulp.src(['tests/test-pages/common/**.css', 'tests/test-pages/common/**.js'])
            .pipe(gulp.dest(paths.flatappBuild + '/static/common'))
    );
});

gulp.task('flatapp:soy', ['flatapp:copy'], function(cb) {
    var opts = {
        baseDir: paths.flatappBuild + '/soy/pages',
        i18nBundle: paths.i18nBundle,
        glob: '**.soy',
        outDir: paths.flatappBuild + '/static/pages',
        rootNamespace: 'testPages.pages',
        data: {
            auiVersion: pkg.version,
            language: 'en'
        },
        dependencies: [{
            // AUI soy templates
            baseDir: 'src/soy',
            glob: '**.soy'
        }, {
            // flatapp dependencies
            baseDir: paths.flatappBuild + '/soy/dependencies',
            glob: '**.soy'
        }]
    };
    soyRender(opts, function(err) {
        console.log('Flatapp soy rendered');
        cb(err);
    });
});

gulp.task('flatapp:webserver', ['flatapp:soy'], function() {
    return gulp.src(paths.flatappBuild + '/static')
        .pipe(webserver({
            livereload: true,
            directoryListing: true,
            open: 'pages/',
            port: 8000
        }));
});

gulp.task('flatapp:watch', function() {
    // TODO: would be nice to move soy copying from flatapp:copy to deps in flatapp:soy, so we could separate reloading into 'copy' and 'soy' only
    gulp.watch(['tests/flatapp/**', 'tests/test-pages/**'], ['flatapp:copy', 'flatapp:soy']);
});

gulp.task('flatapp', ['watch', 'flatapp:watch', 'flatapp:webserver']);
