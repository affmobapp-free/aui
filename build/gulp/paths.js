module.exports = {
    jsSource: 'src/js/',
    jsVendorSource: 'src/js-vendor/',
    bowerSource: 'bower_components/',
    styleSource: 'src/less/',
    cssVendorSource: 'src/css-vendor/',
    soySource: 'src/soy/',
    compiledSoySource: '.tmp/compiled-soy/',
    i18nBundle: 'src/i18n/aui.properties',
    dist: 'dist/',
    tmp: '.tmp/',
    testPageSoySource: 'tests/test-pages/',
    docsDist: 'dist-docs',
    requireJsTransformed: '.tmp/requirejs-transformed/',
    flatappBuild: '.tmp-flatapp'
};
