var gulp = require('gulp'),
    jscs = require('gulp-jscs'),
    jshint = require('gulp-jshint');

gulp.task('lint:jscs', function() {
    return gulp.src(['gulpfile.js', 'src/js/**.js'])
        .pipe(jscs({}));
});

gulp.task('lint:jshint', function() {
    return gulp.src(['gulpfile.js', 'src/js/**.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
});

gulp.task('lint', ['lint:jscs', 'lint:jshint']);