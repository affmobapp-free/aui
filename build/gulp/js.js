var extend = require('node.extend'),
    fs = require('fs-extra'),
    gulp = require('gulp'),
    merge = require('merge-stream'),
    rename = require('gulp-rename'),
    path = require('path'),
    paths = require('./paths'),
    pkg = require('../../package.json'),
    replace = require('gulp-replace'),
    rjs = require('../plugins/gulp-aui-requirejs-2'),
    run = require('gulp-run'),
    soyCompile = require('../tasks/soy-compile'),
    uglify = require('gulp-uglify');

var requireJsConfig = require('../configs/requirejs');

gulp.task('js:i18n', function() {
    return gulp.src(paths.i18nBundle)
        .pipe(run('./build/bin/propertiesToJs.sh', { silent: true }))
        .pipe(rename('aui.properties.js'))
        .pipe(gulp.dest(paths.tmp));
});

gulp.task('js:build', ['js:soyCompile', 'js:i18n'], function() {
    var modulePaths = requireJsConfig.paths;
    // Builds a directory of files that retain the same structure as the originals that have been
    // transformed to include named defines.
    function onBuildWrite(moduleName, modulePath, moduleContent) {
        var realpath = modulePaths[moduleName] || moduleName;
        var original = path.normalize(paths.requireJsTransformed + paths.jsSource + realpath);
        fs.outputFileSync(original + '.js', moduleContent);
        return moduleContent;
    }
    var streams = ['aui', 'aui-css-deprecation-warnings', 'aui-datepicker', 'aui-experimental', 'aui-soy'].map(function(bundle) {
        var cfg = extend({}, requireJsConfig[bundle], {
            out: bundle + '.js',
            onBuildWrite: onBuildWrite
        });
        return rjs(cfg)
            .pipe(replace(/\${project.version}/, pkg.version))
            .pipe(gulp.dest('dist/aui/js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest('dist/aui/js'))
    });
    // Add do-not-use-doge to export only the individual files in onBuildWrite
    var dogeCfg = extend({}, requireJsConfig['DO-NOT-USE-DOGE'], {
        out: 'do-not-use-doge.js',
        onBuildWrite: onBuildWrite
    });
    streams.push(rjs(dogeCfg));
    return merge(streams);
});

gulp.task('js:soyCompile', function(cb) {
    var opts = {
        i18nBundle: paths.i18nBundle,
        baseDir: paths.soySource,
        glob: '**.soy',
        outDir: paths.compiledSoySource
    };
    soyCompile(opts, function(err) {
        if (err) {
            cb(err);
        } else {
            cb();
        }
    });
});

gulp.task('js', ['js:build'], function() {
    return gulp.src('dist/aui/js/**')
        // docs
        .pipe(gulp.dest(paths.docsDist + '/aui/js'))
        // flatapp
        .pipe(gulp.dest(paths.flatappBuild + '/static/common/js'));
});