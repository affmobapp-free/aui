var args = require('./args'),
    extend = require('node.extend'),
    gulp = require('gulp'),
    karma = require('gulp-karma'),
    paths = require('./paths'),
    stringSrc = require('../plugins/gulp-string-src');

var requireJsConfig = require('../configs/requirejs');

gulp.task('test:requireJs-config', function () {
    var cfg = requireJsConfig.testConfig;
    return stringSrc('tests-requirejs-config.js', 'window.requireConfig=' + JSON.stringify(cfg) + ';')
        .pipe(gulp.dest(paths.tmp));
});

gulp.task('test', ['test:requireJs-config', 'js:soyCompile', 'js:i18n'], function() {
    var baseConfig = require('../configs/karma-shared')(args);
    var jqueryVersion = args.jquery || '1.8.3';
    var browsers = (args.browsers && args.browsers.split(' ')) || ['Chrome'];
    var karmaConfig = extend({}, baseConfig, {
        singleRun: !args.debug,
        browsers: browsers,
        junitReporter: {
            outputFile: 'tests/karma-jquery-' + jqueryVersion + '.xml'
        }
    });
    return gulp.src([])
        .pipe(karma(karmaConfig))
        .on('error', function(err) {
            throw err;
        });
});

gulp.task('test:flatpack', ['js', 'css'], function() {
    var baseConfig = require('../configs/karma-shared')(args);
    var jqueryVersion = args.jquery || '1.8.3';
    var karmaConfig = extend({}, baseConfig, {
        junitReporter: {
            outputFile: 'tests/karma-jquery-' + jqueryVersion + '-flatpack.xml'
        },
        singleRun: true,
        files: [
            'src/css/layer.css',
            'src/css/tabs.css',
            'tests/unit/atlassian-js/atlassian-js-test.css',
            'bower_components/skate/dist/skate.js',
                paths.bowerSource + '/jquery/jquery.js',
                paths.bowerSource + '/jquery/dist/jquery.js',
                paths.bowerSource + '/jquery-migrate/jquery-migrate.js',
            {
                pattern: paths.dist + 'aui/js/*.js',
                included: false
            },
            // progress-data-set and restful table need to be included manually because the flatpack does not have it
            {
                pattern: paths.jsSource + 'experimental-autocomplete/*.js',
                included: false
            }, {
                pattern: paths.jsSource + 'experimental-restfultable/*.js',
                included: false
            }, {
                pattern: paths.jsSource + 'experimental-events/events.js',
                included: false
            }, {
                pattern: paths.jsVendorSource + 'backbone/backbone.js',
                included: false
            }, {
                pattern: paths.jsVendorSource + 'underscorejs/underscore.js',
                included: false
            }, {
                pattern: paths.jsVendorSource + 'jquery/serializetoobject.js',
                included: false
            },
            // end progress-data-set and restful table

            // tests
            {
                pattern: 'tests/helpers/all.js',
                included: false
            }, {
                pattern: 'tests/unit/jquery-loader.js',
                included: false
            }, {
                pattern: 'tests/unit/{*,**/*}-test.js',
                included: false
            },
            'tests/unit/karma-flatpack-main.js'
        ]
    });
    return gulp.src([])
        .pipe(karma(karmaConfig))
        .on('error', function(err) {
            throw err;
        });
});
