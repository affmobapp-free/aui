var spawn = require('child_process').spawn;

'use strict';

module.exports = function(opts, cb) {

    var javaExec = process.env.JAVA_HOME ? process.env.JAVA_HOME + '/bin/java' : 'java';
    var cmd = spawn(javaExec, [
        '-jar', './build/jar/atlassian-soy-cli-support-3.2.0-SOY-40-cli-support-1-jar-with-dependencies.jar',
        '--type', 'js',
        '--i18n', opts.i18nBundle,
        '--basedir', opts.baseDir,
        '--glob', opts.glob,
        '--outdir', opts.outDir
    ]);

    cmd.stderr.on('data', function(data) {
        process.stderr.write(data);
    });

    cmd.stdout.on('data', function(data) {
        process.stdout.write(data);
    });

    cmd.on('close', function(code) {
        if (code === 0) {
            cb();
        }
        else {
            cb(code);
        }
    });
};
