var extend = require('node.extend'),
    fs = require('fs'),
    gulp = require('gulp'),
    frontMatter = require('gulp-front-matter'),
    gulpsmith = require('gulpsmith'),
    path = require('path'),
    merge = require('merge-stream'),
    metalSmithLayouts = require('metalsmith-layouts'),
    metalSmithMarkdown = require('metalsmith-markdown'),
    metalSmithTextReplace = require('metalsmith-text-replace'),
    rjs = require('../plugins/gulp-aui-requirejs-2');

module.exports = function createDocsGulpTask(o) {

    return merge(
        rjs(o.requireJsConfig)
            .pipe(gulp.dest(o.out)),

        // metalsmith html
        gulp.src(o.templateSrc)
            .pipe(frontMatter()).on('data', function(file) {
                extend(file, file.frontMatter);
                delete file.frontMatter;
            })
            .pipe(gulpsmith()
                .use(metalSmithMarkdown())
                .use(metalSmithLayouts({
                    engine: 'handlebars',
                    pattern: '**/**.html',
                    directory: 'docs/layouts'
                }))
                .use(metalSmithTextReplace({
                    '**/**.html': [{
                        find: /\${project.version}/g,
                        replace: o.pkgVersion
                    }, {
                        find: /\${dist.location}/g,
                        replace: o.distLocation
                    }]
                })))
            .pipe(gulp.dest(o.out)),

        // copy css / images
        gulp.src(o.assetSrc)
            .pipe(gulp.dest(o.out))
    );
};
