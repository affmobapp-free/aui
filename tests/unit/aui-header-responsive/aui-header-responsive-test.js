/* jshint expr:true */
define([
    '../../helpers/all.js',
    '../../../bower_components/skatejs/dist/skate.js',
    '../../../bower_components/skatejs-template-html/dist/template-html.js',
    'jquery',
    'aui-header-responsive',
    'soy/page',
    'dropdown2'
], function (
    helpers,
    skate,
    template,
    $
) {
    'use strict';

    var template = window.skateTemplateHtml;

    describe('Responsive header - ', function () {
        var $window = $(window);
        var header;

        beforeEach(function () {
            header = helpers.ensureHtmlElement('<aui-header></aui-header>');
            header.id = 'test-header';
            header.setAttribute('responsive', 'true');
            skate.init(header);
            template.wrap(header).innerHTML = '' +
              '<ul class="aui-nav aui-header-content">' +
                  '<li id="item-1" class="test-header-item"><a class="aui-nav-link" href="#">Some long text</a></li>' +
                  '<li id="item-2" class="test-header-item"><a class="aui-nav-link" href="#">Some long text</a></li>' +
                  '<li id="item-3" class="test-header-item"><a class="aui-nav-link" href="#">Some long text</a></li>' +
                  '<li id="item-4" class="test-header-item"><a class="aui-nav-link" href="#">Some long text</a></li>' +
              '</ul>';

            helpers.fixtures({
                header: header
            });
        });

        afterEach(function () {
            $('#test-fixture').width('auto');
        });

        function resizeWindow (width) {
            $('#test-fixture').width(width);
            $window.trigger('resize');
        }

        function isInResponsiveDropdown (selector) {
            return $('#aui-responsive-header-dropdown-list-0').children(selector).length === 1;
        }


        function isInHeader (selector) {
            return $('#test-header .aui-header-primary .aui-nav').children(selector).length === 1;
        }

        describe('when all items fit and when the container is resized smaller', function () {
            // More menu width is always the same. Unable to grab the width dynamically because the more menu is
            // inserted after the resize not before, hence calling width will get an incorrect value.
            var moreMenuWidth = 100;
            beforeEach(function() {
                var itemWidth = document.querySelector('.test-header-item').offsetWidth;
                var logo = $('#logo');
                var padding = logo.offset().left * 2 + logo.outerWidth(true) + moreMenuWidth;
                resizeWindow(itemWidth * 2 + padding);
            });

            it('the trigger for the responsive menu should be visible', function() {
                expect(document.querySelector('#aui-responsive-header-dropdown-0 > a')).to.be.visible;
            });

            it('the responsive menu contains items 3 and 4', function () {
                expect(isInResponsiveDropdown('#item-3')).to.equal(true);
                expect(isInResponsiveDropdown('#item-4')).to.equal(true);
            });

            it('the header still contains items 1 and 2', function () {
                expect(isInHeader('#item-1')).to.equal(true);
                expect(isInHeader('#item-2')).to.equal(true);
            });

            it('the header does not contain items 3 and 4 ', function () {
                expect(isInHeader('#item-3')).to.equal(false);
                expect(isInHeader('#item-4')).to.equal(false);
            });

            it('the responsive menu does not contain items 1 and 2', function () {
                expect(isInResponsiveDropdown('#item-1')).to.equal(false);
                expect(isInResponsiveDropdown('#item-2')).to.equal(false);
            });
        });

        describe('when only some items fit and when the container is resized larger', function () {
            beforeEach(function () {
                var itemWidth = document.querySelector('.test-header-item').offsetWidth;
                resizeWindow(itemWidth * 4);

                //couldn't find a reliable way that works cross browser (auto and 100% didn't seem to work in phantomJS)
                //so we just set it to something very large
                resizeWindow('9999');
            });

            it('the responsive menu trigger should not be visible', function (){
                expect(document.querySelector('#aui-responsive-header-dropdown-0 > a')).to.not.be.visible;
            });

            it('the header contains items 1, 2, 3, and 4', function (){
                expect(isInHeader('#item-1')).to.equal(true);
                expect(isInHeader('#item-2')).to.equal(true);
                expect(isInHeader('#item-3')).to.equal(true);
                expect(isInHeader('#item-4')).to.equal(true);
            });
        });
    });
});
