define(['soy/dropdown2'], function() {
    "use strict";

    function templates(idPrefix) {
        return {
            legacy: {
                plainSection:
                    '<div id="' + idPrefix + 'section1" class="aui-dropdown2-section">' +
                        '<ul>' +
                        '<li><a id="' + idPrefix + 'item1">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'item2">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'item3">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'item4">Menu item</a></li>' +
                        '</ul>' +
                        '</div>',

                plainSection2:
                    '<div id="' + idPrefix + 'section2" class="aui-dropdown2-section">' +
                        '<ul>' +
                        '<li><a id="' + idPrefix + 'item12">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'item22">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'item32">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'item42">Menu item</a></li>' +
                        '</ul>' +
                        '</div>',

                hiddenSection:
                    '<div id="' + idPrefix + 'section3" class="aui-dropdown2-section">' +
                        '<ul>' +
                        '<li class="aui-dropdown2-hidden"><a id="' + idPrefix + 'hidden1-unchecked-disabled" class="aui-dropdown2-checkbox interactive disabled" >Menu item</a></li>' +
                        '<li class="aui-dropdown2-hidden"><a id="' + idPrefix + 'hidden2-checked" class="aui-dropdown2-checkbox interactive aui-dropdown2-checked">Menu item</a></li>' +
                        '</ul>' +
                        '</div>',

                interactiveSection:
                    '<div id="' + idPrefix + 'section4" class="aui-dropdown2-section">' +
                        '<ul role="radiogroup">' +
                        '<li><a id="' + idPrefix + 'iradio1-interactive-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'iradio2-interactive-unchecked" class="aui-dropdown2-radio interactive">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'iradio3-unchecked" class="aui-dropdown2-radio">Menu item</a></li>' +
                        '</ul>' +
                        '</div>',

                radioSection:
                    '<div id="' + idPrefix + 'section2" class="aui-dropdown2-section">' +
                        '<ul role="radiogroup">' +
                        '<li><a id="' + idPrefix + 'radio1-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'radio2-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked aui-dropdown2-checked">Menu item</a></li>' +
                        '<li><a id="' + idPrefix + 'radio3-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
                        '</ul>' +
                        '</div>',

                checkboxSection:
                    '<div id="' + idPrefix + '" class="aui-dropdown2-section">' +
                        '<ul>' +
                        '<li><a id="'+ idPrefix + 'check1-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
                        '<li><a id="'+ idPrefix + 'check2-checked" class="aui-dropdown2-checkbox interactive checked aui-dropdown2-checked">Menu item</a></li>' +
                        '<li><a id="'+ idPrefix + 'check3-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
                        '</ul>' +
                        '</div>',

                submenuSection:
                    '       <div class="aui-dropdown2-section">' +
                    '           <ul class="aui-list-truncate">' +
                    '               <li>' +
                    '                   <a id="' + idPrefix + 'dd2-menu-1-child-1">Dummy item 1</a>' +
                    '               </li>' +
                    '               <li>' +
                    '                   <a id="' + idPrefix + 'dd2-menu-1-child-2" href="#' + idPrefix + 'dd2-menu-2" aria-controls="' + idPrefix + 'dd2-menu-2" aria-haspopup="true" class="interactive aui-dropdown2-sub-trigger aui-style-default">Open submenu level 1</a>' +
                    '                   <div id="' + idPrefix + 'dd2-menu-2" class="aui-dropdown2 aui-style-default aui-dropdown2-sub-menu" aria-hidden="true">' +
                    '                       <ul class="aui-list-truncate">' +
                    '                           <li>' +
                    '                               <a id="' + idPrefix + 'dd2-menu-2-child-1" class="">Dummy item 2</a>' +
                    '                           </li>' +
                    '                           <li>' +
                    '                               <a id="' + idPrefix + 'dd2-menu-2-child-2" href="#' + idPrefix + 'dd2-menu-3" aria-controls="' + idPrefix + 'dd2-menu-3" aria-haspopup="true" class="interactive aui-dropdown2-sub-trigger aui-style-default">Open submenu level 2</a>' +
                    '                               <div id="' + idPrefix + 'dd2-menu-3" class="aui-dropdown2 aui-style-default aui-dropdown2-sub-menu" aria-hidden="true">' +
                    '                                   <ul class="aui-list-truncate">' +
                    '                                       <li>' +
                    '                                           <a>Final level</a>' +
                    '                                       </li>' +
                    '                                   </ul>' +
                    '                               </div>' +
                    '                           </li>' +
                    '                       </ul>' +
                    '                   </div>' +
                    '               </li>' +
                    '           </ul>' +
                    '       </div>'

            },
            accessible: {
                plainSection: aui.dropdown2.itemGroup({
                    id: idPrefix + '-link-section',
                    items: [
                        {text: 'Menu item', id: idPrefix + 'item1'},
                        {text: 'Menu item', id: idPrefix + 'item2'},
                        {text: 'Menu item', id: idPrefix + 'item3'},
                        {text: 'Menu item', id: idPrefix + 'item4'}
                    ]
                }),

                plainSection2: aui.dropdown2.itemGroup({
                    id: idPrefix + '-link-section-2',
                    items: [
                        {text: 'Menu item', id: idPrefix + 'item21'},
                        {text: 'Menu item', id: idPrefix + 'item22'},
                        {text: 'Menu item', id: idPrefix + 'item23'},
                        {text: 'Menu item', id: idPrefix + 'item24'}
                    ]
                }),

                hiddenSection: aui.dropdown2.itemGroup({
                    id: idPrefix + '-hidden-section',
                    items: [
                        {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'hidden1-unchecked-disabled', isInteractive: true, isDisabled: true, isHidden: true},
                        {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'hidden2-checked', isInteractive: true, isChecked: true, isHidden: true}
                    ]
                }),

                interactiveSection: aui.dropdown2.itemGroup({
                    id: idPrefix + '-interactive-section',
                    items: [
                        {itemType: 'radio', text: 'Menu item', id: idPrefix + 'iradio1-interactive-checked', isInteractive: true, isChecked: true},
                        {itemType: 'radio', text: 'Menu item', id: idPrefix + 'iradio2-interactive-unchecked', isInteractive: true},
                        {itemType: 'radio', text: 'Menu item', id: idPrefix + 'iradio3-unchecked'}
                    ]
                }),

                radioSection: aui.dropdown2.itemGroup({
                    id: idPrefix + '-radio-section',
                    items: [
                        {itemType: 'radio', text: 'Menu item', id: idPrefix + 'radio1-unchecked', isInteractive: true},
                        {itemType: 'radio', text: 'Menu item', id: idPrefix + 'radio2-checked', isInteractive: true, isChecked: true},
                        {itemType: 'radio', text: 'Menu item', id: idPrefix + 'radio3-unchecked', isInteractive: true}
                    ]
                }),

                checkboxSection: aui.dropdown2.itemGroup({
                    id: idPrefix + '-checkbox-section',
                    items: [
                        {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'check1-unchecked', isInteractive: true},
                        {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'check2-checked', isInteractive: true, isChecked: true},
                        {itemType: 'checkbox', text: 'Menu item', id: idPrefix + 'check3-unchecked', isInteractive: true}
                    ]
                })
            }
        };
    }

    return templates;
});