/* jshint expr:true */
define([
    '../../helpers/all.js',
    'test-text!./sidebar.html',
    'test-text!./sidebar-no-submenus.html',
    'aui-sidebar'
], function (
    helpers,
    sidebarHtml,
    sidebarHtmlNoSubmenus
) {
    'use strict';

    describe('Sidebar', function () {
        var sidebar;
        var $pageActions;
        var $avatar;
        var clock;

        beforeEach(function () {
            $('#test-fixture').html(sidebarHtml);

            sidebar = new AJS.sidebar(AJS.$('.aui-sidebar'));
            clock = sinon.useFakeTimers();
            $pageActions = $('#test-sidebar-page-actions');
            $avatar = $('#test-sidebar-avatar');
        });

        afterEach(function () {
            // no unattended tooltips between tests
            $('.aui-sidebar-section-tooltip').remove();
            clock.restore();
            sidebar._remove();
        });

        it('puts a class on the body', function () {
            expect($(document.body).hasClass('aui-page-sidebar')).to.be.true;
        });

        it('can be collapsed and uncollapsed via the .collapse() and .expand() methods', function () {
            sidebar.collapse();
            expect(sidebar.isCollapsed()).to.be.true;
            sidebar.collapse();
            expect(sidebar.isCollapsed()).to.be.true;
            sidebar.expand();
            expect(sidebar.isCollapsed()).to.be.false;
        });

        it('should appropriately collapse/expand when calling toggle()', function () {
            var isCollapsed = sidebar.isCollapsed();

            sidebar.toggle();
            expect(sidebar.isCollapsed()).to.equal(isCollapsed = !isCollapsed);
            sidebar.toggle();
            expect(sidebar.isCollapsed()).to.equal(!isCollapsed);
        });

        it('should collapse/expand when the toggle icon is clicked', function () {
            var $toggle = sidebar.$el.find('.aui-sidebar-toggle');
            var isCollapsed = sidebar.isCollapsed();

            $toggle.click();
            expect(sidebar.isCollapsed()).to.equal(isCollapsed = !isCollapsed);
            $toggle.click();
            expect(sidebar.isCollapsed()).to.equal(!isCollapsed);
        });

        it('should set aria-expanded when collapsed/expanded', function () {
            sidebar.collapse();
            expect(sidebar.$el.attr('aria-expanded')).to.equal('false');
            sidebar.expand();
            expect(sidebar.$el.attr('aria-expanded')).to.equal('true');
        });

        it('should toggle the aui-sidebar-collapsed class on the <body>', function () {
            sidebar.collapse();
            expect(AJS.$('body').hasClass('aui-sidebar-collapsed')).to.be.true;
            sidebar.expand();
            // The sidebar may be in fly-out mode if the browser is too narrow,
            // in which case aui-sidebar-collapsed remains on the <body>.
            expect(AJS.$('body').hasClass('aui-sidebar-collapsed')).to.equal(sidebar.isViewportNarrow());
        });

        it('should be automatically collapsed/expanded when the browser is resized', function () {
            sidebar.reflow(0, 1000, 2000);
            sidebar.expand();
            sidebar.reflow(0, 1000, 1024);
            expect(sidebar.isCollapsed()).to.be.true;
            sidebar.reflow(0, 1000, 2000);
            expect(sidebar.isCollapsed()).to.be.false;
        });

        it('should not be automatically expanded when the browser is resized and event is prevented', function () {
            sidebar.reflow(0, 1000, 2000);
            sidebar.expand();

            sidebar.on('expand-start', function (e) {
                e.preventDefault();
            });

            sidebar.reflow(0, 1000, 1024);
            expect(sidebar.isCollapsed()).to.be.true;
            sidebar.reflow(0, 1000, 2000);
            expect(sidebar.isCollapsed()).to.be.true;
        });

        it('should not be automatically collapsed when the browser is resized and event is prevented', function () {
            sidebar.reflow(0, 1000, 2000);
            sidebar.expand();

            sidebar.on('collapse-start', function (e) {
                e.preventDefault();
            });

            sidebar.reflow(0, 1000, 1024);
            expect(sidebar.isCollapsed()).to.be.false;
            sidebar.reflow(0, 1000, 2000);
            expect(sidebar.isCollapsed()).to.be.false;
        });

        it('should remain collapsed when collapsed at wide width and browser made wider', function () {
            sidebar.reflow(0, 1000, 2000);
            sidebar.collapse();
            expect(sidebar.isCollapsed()).to.be.true;
            sidebar.reflow(0, 1000, 3000);
            expect(sidebar.isCollapsed()).to.be.true;
        });

        it('should expand in fly-out mode when expanded in a narrow browser', function () {
            sidebar.collapse();
            sidebar.reflow(0, 1000, 1024);
            sidebar.expand();
            expect(sidebar.$el.hasClass('aui-sidebar-fly-out')).to.be.true;

            sidebar.collapse();
            sidebar.reflow(0, 1000, 2000);
            sidebar.expand();
            expect(sidebar.$el.hasClass('aui-sidebar-fly-out')).to.be.false;
        });

        it('should keep events after interacting with expanded/collapsed sidebar', function () {
            var targetEventsSelector = ".aui-sidebar ul.aui-nav li ul.aui-nav li",
                clickEvent = sinon.spy();

            // initial state, sidebar is expanded
            sidebar.expand();
            expect(sidebar.isCollapsed()).to.be.false;

            // counter should be zero because there is not event to be triggered
            AJS.$(targetEventsSelector).eq(0).click();
            expect(clickEvent.calledOnce).to.be.false;
            clickEvent.reset();

            // add event and assure it is working
            AJS.$(targetEventsSelector).on("click", clickEvent);
            AJS.$(targetEventsSelector).eq(0).click();
            expect(clickEvent.calledOnce).to.be.true;
            clickEvent.reset();

            sidebar.collapse();
            AJS.$(targetEventsSelector).eq(0).click();
            expect(clickEvent.calledOnce).to.be.true;
            clickEvent.reset();

            // initiate interaction with submenus, which should trigger an inline-dialog to show up
            helpers.focus($(sidebar.collapsedTriggersSelector));
            AJS.$(targetEventsSelector).eq(0).click();
            expect(clickEvent.calledOnce).to.be.true;
            clickEvent.reset();

            sidebar.expand();
            AJS.$(targetEventsSelector).eq(0).click();
            expect(clickEvent.calledOnce).to.be.true;
            clickEvent.reset();
        });

        describe('JavaScript events', function () {
            var collapseStart = sinon.spy();
            var collapseEnd = sinon.spy();
            var expandStart = sinon.spy();
            var expandEnd = sinon.spy();

            function resetSpies() {
                collapseStart.reset();
                collapseEnd.reset();
                expandStart.reset();
                expandEnd.reset();
            }

            function expectSpiesToBeCalledOnce() {
                expect(expandStart.calledOnce).to.be.true;
                expect(expandEnd.calledOnce).to.be.true;
                expect(collapseStart.calledOnce).to.be.true;
                expect(collapseEnd.calledOnce).to.be.true;
            }

            beforeEach(function () {
                sidebar.on('collapse-start', collapseStart);
                sidebar.on('collapse-end', collapseEnd);
                sidebar.on('expand-start', expandStart);
                sidebar.on('expand-end', expandEnd);
            });

            it('should fire when collapsed', function () {
                sidebar.expand();
                resetSpies();

                sidebar.collapse();
                expect(collapseStart.calledOnce).to.be.true;
                expect(collapseEnd.calledOnce).to.be.true;
                expect(expandStart.callCount).to.equal(0);
                expect(expandEnd.callCount).to.equal(0);
                expect(collapseStart.calledBefore(collapseEnd)).to.be.true;
            });

            it('should not fire when already collapsed', function () {
                sidebar.collapse();
                resetSpies();

                sidebar.collapse();
                expect(collapseStart.callCount).to.equal(0);
                expect(collapseEnd.callCount).to.equal(0);
                expect(expandStart.callCount).to.equal(0);
                expect(expandEnd.callCount).to.equal(0);
            });

            it('should fire when expanded', function () {
                sidebar.collapse();
                resetSpies();

                sidebar.expand();
                expect(collapseStart.callCount).to.equal(0);
                expect(collapseEnd.callCount).to.equal(0);
                expect(expandStart.calledOnce).to.be.true;
                expect(expandEnd.calledOnce).to.be.true;
                expect(expandStart.calledBefore(expandEnd));
            });

            it('should not fire when already expanded', function () {
                sidebar.expand();
                resetSpies();

                sidebar.expand();
                expect(collapseStart.callCount).to.equal(0);
                expect(collapseEnd.callCount).to.equal(0);
                expect(expandStart.callCount).to.equal(0);
                expect(expandEnd.callCount).to.equal(0);
            });

            it('expand-start should be preventable', function () {
                sidebar.collapse();
                resetSpies();

                sidebar.on('expand-start', function (e) {
                    e.preventDefault();
                });

                sidebar.expand();

                expect(expandStart.calledOnce).to.be.true;
                expect(expandEnd.callCount).to.equal(0);
            });

            it('collapse-start should be preventable', function () {
                sidebar.expand();
                resetSpies();

                sidebar.on('collapse-start', function (e) {
                    e.preventDefault();
                });

                sidebar.collapse();

                expect(collapseStart.calledOnce).to.be.true;
                expect(collapseEnd.callCount).to.equal(0);
            });

            it('should have isResponsive property set to false when expanded/collapsed with click', function () {
                var $toggle = sidebar.$el.find('.aui-sidebar-toggle');

                var numberOfIsResponsiveAssertions = 0;
                sidebar.on('expand-start expand-end collapse-start collapse-end', function (e) {
                    expect(e.isResponsive).to.be.false;
                    numberOfIsResponsiveAssertions += 1;
                });
                resetSpies();
                $toggle.click();
                $toggle.click();

                expectSpiesToBeCalledOnce();
                expect(numberOfIsResponsiveAssertions).to.equal(4);
            });

            it('should have isResponsive property set to false when expanded/collapsed with API', function () {
                var numberOfIsResponsiveAssertions = 0;
                sidebar.on('expand-start expand-end collapse-start collapse-end', function (e) {
                    expect(e.isResponsive).to.be.false;
                    numberOfIsResponsiveAssertions += 1;
                });
                resetSpies();
                sidebar.expand();
                sidebar.collapse();

                expectSpiesToBeCalledOnce();
                expect(numberOfIsResponsiveAssertions).to.equal(4);
            });

            it('should have isResponsive property set to true when expanded/collapsed via viewport size change', function () {
                var numberOfIsResponsiveAssertions = 0;
                sidebar.reflow(0, 1000, 2000);
                sidebar.expand();
                resetSpies();

                sidebar.on('expand-start expand-end collapse-start collapse-end', function (e) {
                    expect(e.isResponsive).to.be.true;
                    numberOfIsResponsiveAssertions += 1;
                });

                sidebar.reflow(0, 1000, 1024);
                sidebar.reflow(0, 1000, 2000);

                expectSpiesToBeCalledOnce();
                expect(numberOfIsResponsiveAssertions).to.equal(4);
            });
        });

        describe('javascript API for sidebar.submenus', function () {
            var $submenuTrigger;
            var $noSubmenuTrigger;

            function getVisibleSubmenus() {
                return $('.aui-sidebar-submenu').filter(':visible').filter(function() {
                    var allParentsVisible = $(this).parents().filter(function() {
                        return $(this).attr('aria-hidden') === 'true';
                    }).length === 0;
                    return allParentsVisible;
                });
            }

            beforeEach(function() {
                sidebar.collapse();
                $submenuTrigger = $('#test-submenu-trigger');
                $noSubmenuTrigger = $('#test-no-submenu-trigger');
            });

            it('.submenu() gets the submenu for a trigger', function () {
                expect(sidebar.submenus.submenu($submenuTrigger).length).to.be.above(0);
            });

            it('.hasSubmenu() returns whether the trigger should have an inline dialog shown in the collapsed state', function () {
                expect(sidebar.submenus.hasSubmenu($submenuTrigger)).to.be.true;
                expect(sidebar.submenus.hasSubmenu($noSubmenuTrigger)).to.be.false;
            });

            it('.submenuHeadingHeight() returns a number', function () {
                expect(sidebar.submenus.submenuHeadingHeight()).to.be.a('number');
            });

            it('.isShowing() returns true if a submenu trigger is hovered over', function() {
                expect(sidebar.submenus.isShowing()).to.be.false;
                helpers.focus($submenuTrigger);
                expect(sidebar.submenus.isShowing()).to.be.true;
            });

            it('.show() shows a submenu', function() {
                var e = new CustomEvent('mouseover');
                e.target = $submenuTrigger[0];

                expect(sidebar.submenus.isShowing()).to.be.false;
                sidebar.submenus.show(e, $submenuTrigger[0]);
                expect(sidebar.submenus.isShowing()).to.be.true;
            });

            it('.hide() hides currently showing submenus', function () {
                helpers.focus($submenuTrigger);
                clock.tick(500);
                expect(getVisibleSubmenus().length).to.equal(1);
                sidebar.submenus.hide();
                clock.tick(500);
                expect(getVisibleSubmenus().length).to.equal(0);
            });

            it('inline dialog show / hide handler functions exist', function () {
                expect(sidebar.submenus.inlineDialogShowHandler).to.be.a('function');
                expect(sidebar.submenus.inlineDialogHideHandler).to.be.a('function');
            });

            it('inline dialog moveSubmenuToInlineDialog / restoreSubmenu functions exist', function () {
                expect(sidebar.submenus.moveSubmenuToInlineDialog).to.be.a('function');
                expect(sidebar.submenus.restoreSubmenu).to.be.a('function');
            });
        });

        it('should be considered narrow or wide at a certain px width', function () {
            [0, 1024, 1239].forEach(function (width) {
                expect(sidebar.isViewportNarrow(width)).to.equal(true);
            });
            [1240, 1280, 1440].forEach(function (width) {
                expect(sidebar.isViewportNarrow(width)).to.equal(false);
            });
        });
    });

    describe('Sidebar with no submenus', function() {
        var sidebar;
        var $pageActions;
        var $avatar;
        var clock;

        beforeEach(function () {
            $('#test-fixture').html(sidebarHtmlNoSubmenus);

            sidebar = new AJS.sidebar(AJS.$('.aui-sidebar'));

            clock = sinon.useFakeTimers();

            $pageActions = $('#test-sidebar-page-actions');
            $avatar = $('#test-sidebar-avatar');
        });

        afterEach(function () {
            clock.restore();
        });

        it('Sidebar.submenus.isShowing() returns false', function() {
            expect(sidebar.submenus.isShowing()).to.be.false;
        });
    });
});
