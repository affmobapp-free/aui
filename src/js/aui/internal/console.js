window.AJS = window.AJS || {};

(function() {
    'use strict';

    /**
     * AUI-2773
     * The following shim for console is deprecated and to be removed in AUI 6.
     * We shouldn't be creating console.log if it doesn't exist; instead, we should avoid using it directly.
     * @start deprecated
     */
    if (typeof window.console === 'undefined') {
        window.console = {
            messages: [],

            log: function (text) {
                this.messages.push(text);
            },

            show: function () {
                alert(this.messages.join('\n'));
                this.messages = [];
            }
        };
    } else {
        // Firebug console - show not required to do anything.
        window.console.show = function () {};
    }
    /** @end deprecated */


    function polyfillConsole (prop) {
        return function () {
            if (typeof console !== 'undefined' && console[prop]) {
                Function.prototype.apply.call(console[prop], console, arguments);
            }
        };
    }

    /**
     * Logs the given object to the console.
     */
    AJS.log = polyfillConsole('log');

    /**
     * Logs the given object to the console as a warning.
     */
    AJS.warn = polyfillConsole('warn');

    /**
     * Logs the given object to the console as an error.
     */
    AJS.error = polyfillConsole('error');

})();
